<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tests', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('full_name');
            $table->string('fathers_name');
            $table->string('grandfathers_name');
            $table->string('place_of_birth');
            $table->date('date_of_birth');
            $table->string('sex');
            $table->string('address');
            $table->string('religion');
            $table->string('nationality');
            $table->integer('height');
            $table->integer('weight');
            $table->string('passport_no');
            $table->string('place_of_issue');
            $table->date('date_of_expiry');
            $table->string('martial_status');
            $table->string('child');
            $table->integer('mobile_no');
            $table->string('email_address');
            $table->string('social_media');
            $table->string('emergency_contact');
            $table->integer('mobile_no_emergency');
            $table->string('last1');
            $table->string('last2');
            $table->string('level');
            $table->string('name_of_institution');
            $table->string('city');
            $table->string('mount1');
            $table->string('mount2');
            $table->string('major');
            $table->string('photo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tests');
    }
}
