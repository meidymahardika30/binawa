<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Test;

class TestController extends Controller
{
    public function store(Request $request)
    {
        $store = new Test;
        $store->full_name = $request->full_name;
        $store->fathers_name = $request->fathers_name;
        $store->grandfathers_name = $request->grandfathers_name;
        $store->place_of_birth = $request->place_of_birth;
        $store->date_of_birth = $request->date_of_birth;
        $store->sex = $request->sex;
        $store->address = $request->address;
        $store->religion = $request->religion;
        $store->nationality = $request->nationality;
        $store->height = $request->height;
        $store->weight = $request->weight;
        $store->passport_no = $request->passport_no;
        $store->place_of_issue = $request->place_of_issue;
        $store->date_of_expiry = $request->date_of_expiry;
        $store->martial_status = $request->martial_status;
        $store->child = $request->child;
        $store->mobile_no = $request->mobile_no;
        $store->email_address = $request->email_address;
        $store->social_media = $request->social_media;
        $store->emergency_contact = $request->emergency_contact;
        $store->mobile_no_emergency = $request->mobile_no_emergency;
        $store->last1 = $request->last1;
        $store->last2 = $request->last2;
        $store->level = $request->level;
        $store->name_of_institution = $request->name_of_institution;
        $store->city = $request->city;
        $store->mount1 = $request->mount1;
        $store->mount2 = $request->mount2;
        $store->major = $request->major;
        $store->photo = $request->photo;

        if($store->save()){
            return redirect('/show');
        }else{
            return redirect('/home');
        }
    }

    public function show(){
        $data = Test::all();

        return view('show', compact('data'));
    }
}
