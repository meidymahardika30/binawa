@extends('layouts.app')

@section('content')
<form action="{{url('/store')}}" method="post">
    @csrf
<div class="container">
                <table>
                    <tr>
                        <td>
                            Full Name
                        </td>
                        <td>
                            :
                        </td>
                        <td>
                            <input type="text" name="full_name"> (As In Passport or IC/KTP)
                        </td>
                    </tr>

                    <tr>
                        <td>
                            Father's Name
                        </td>
                        <td>
                            :
                        </td>
                        <td>
                            <input type="text" name="fathers_name">
                        </td>
                    </tr>

                    <tr>
                        <td>
                            Grand Father's Name
                        </td>
                        <td>
                            :
                        </td>
                        <td>
                            <input type="text" name="grandfathers_name">
                        </td>
                    </tr>

                    <tr>
                        <td>
                            Place Of Birth
                        </td>
                        <td>
                            :
                        </td>
                        <td>
                            <input type="text" name="place_of_birth">
                        </td>
                    </tr>

                    <tr>
                        <td>
                            Date Of Birth
                        </td>
                        <td>
                            :
                        </td>
                        <td>
                            <input type="date" name="date_of_birth">
                        </td>
                    </tr>

                    <tr>
                        <td>
                            Sex
                        </td>
                        <td>
                            :
                        </td>
                        <td>
                            <input type="checkbox" name="sex" value="Male">Male
                            <input type="checkbox" name="sex" value="Female">Female
                        </td>
                    </tr>

                    <tr>
                        <td>
                            Address
                        </td>
                        <td>
                            :
                        </td>
                        <td>
                            <textarea rows="3" cols="40" name="address">
                                
                            </textarea>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            Religion
                        </td>
                        <td>
                            :
                        </td>
                        <td>
                            <input type="text" name="religion">
                        </td>
                    </tr>

                    <tr>
                        <td>
                            Nationality
                        </td>
                        <td>
                            :
                        </td>
                        <td>
                            <input type="text" name="nationality">
                        </td>
                    </tr>

                    <tr>
                        <td>
                            Height
                        </td>
                        <td>
                            :
                        </td>
                        <td>
                            <input type="number" name="height"> cm
                        </td>
                    </tr>

                    <tr>
                        <td>
                            Weight
                        </td>
                        <td>
                            :
                        </td>
                        <td>
                            <input type="number" name="weight"> kg
                        </td>
                    </tr>

                    <tr>
                        <td>
                            Passport No
                        </td>
                        <td>
                            :
                        </td>
                        <td>
                            <input type="number" name="passport_no">
                        </td>
                    </tr>

                    <tr>
                        <td>
                            Place Of Issue
                        </td>
                        <td>
                            :
                        </td>
                        <td>
                            <input type="text" name="place_of_issue">
                        </td>
                    </tr>

                    <tr>
                        <td>
                            Date Of Expiry
                        </td>
                        <td>
                            :
                        </td>
                        <td>
                            <input type="date" name="date_of_expiry">
                        </td>
                    </tr>

                    <tr>
                        <td>
                            Martial Status
                        </td>
                        <td>
                            :
                        </td>
                        <td>
                            <input type="checkbox" name="martial_status">Single
                            <input type="checkbox" name="martial_status">Married
                            <input type="checkbox" name="martial_status">Widowed
                            <input type="checkbox" name="martial_status">Divorced
                            With <input type="number" name="child"> Child(Children)
                        </td>
                    </tr>

                    <tr>
                        <td>
                            Mobile No
                        </td>
                        <td>
                            :
                        </td>
                        <td>
                            <input type="number" name="mobile_no">
                        </td>
                    </tr>

                    <tr>
                        <td>
                            E-mail Address
                        </td>
                        <td>
                            :
                        </td>
                        <td>
                            <input type="email" name="email_address">
                        </td>
                    </tr>

                    <tr>
                        <td>
                            Social Media
                        </td>
                        <td>
                            :
                        </td>
                        <td>
                            <input type="text" name="social_media">
                        </td>
                    </tr>

                    <tr>
                        <td>
                            Emergency Contact Name/Relationship
                        </td>
                        <td>
                            :
                        </td>
                        <td>
                            <input type="text" name="emergency_contact"> Mobile No : <input type="text" name="mobile_no_emergency">
                        </td>
                    </tr>

                    <tr>
                        <td>
                            Last Education GPA
                        </td>
                        <td>
                            :
                        </td>
                        <td>
                            <input type="text" name="last1"> Out Of <input type="text" name="last2"> , with Education details as Follow:
                        </td>
                    </tr>

                    <tr>
                        <td>
                            Level
                        </td>
                        <td>
                            :
                        </td>
                        <td>
                            <select name="level">
                                <option></option>
                                <option>Bachelor</option>
                                <option>Diploma</option>
                                <option>High School</option>
                            </select>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            NAME OF INSTITUTION
                        </td>
                        <td>
                            :
                        </td>
                        <td>
                            <input type="text" name="name_of_institution">
                        </td>
                    </tr>

                    <tr>
                        <td>
                            CITY & COUNTRY
                        </td>
                        <td>
                            :
                        </td>
                        <td>
                            <input type="text" name="city">
                        </td>
                    </tr>

                    <tr>
                        <td>
                            MONTH/YEAR
                        </td>
                        <td>
                            :
                        </td>
                        <td>
                            FROM <input type="text" name="mount1"> TO <input type="text" name="mount2">
                        </td>
                    </tr>

                    <tr>
                        <td>
                            MAJOR
                        </td>
                        <td>
                            :
                        </td>
                        <td>
                            <input type="text" name="major">
                        </td>
                    </tr>

                    <tr>
                        <td>
                            Photo
                        </td>
                        <td>
                            :
                        </td>
                        <td>
                            <input type="file" name="photo">
                        </td>
                    </tr>

                    <tr>
                        <td></td>
                        <td></td>
                        <td>
                            <input type="submit" name="submit" value="SAVE">
                        </td>
                    </tr>

                </table>
</div>
</form>
@endsection
