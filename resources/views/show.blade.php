@extends('layouts.app')

@section('content')
<form action="{{url('/store')}}" method="post">
    @csrf
<div class="container">
    <table border="1">
        <tr>
            <td>Full Name</td>
            <td>Fathers Name</td>
            <td>Grandfathers Name</td>
        </tr>
        @foreach($data as $val)
        <tr>
            <td>{{ $val->full_name }}</td>
            <td>{{ $val->fathers_name }}</td>
            <td>{{ $val->grandfathers_name }}</td>
        </tr>
        @endforeach
    </table>
</div>
</form>
@endsection
